#!/bin/bash
source src.sh

export DATA_SERVER="$SRC_DIR/evolution-data-server"
export EVOLUTION="$SRC_DIR/evolution"
export EWS="$SRC_DIR/evolution-ews"

mkdir -p "$DATA_SERVER/_build"
mkdir -p "$EVOLUTION/_build"
mkdir -p "$EWS/_build"

cp "$SRC_DIR/example/data-server-build.sh" "$DATA_SERVER/_build/build.sh"
cp "$SRC_DIR/example/evolution-build.sh" "$EVOLUTION/_build/build.sh"
cp "$SRC_DIR/example/evolution-ews.sh" "$EWS/_build/build.sh"
