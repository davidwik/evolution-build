#!/bin/bash
if [ -z $PREFIX ]
then
    echo "Need to source the src.sh first"
    exit 1
fi

cmake .. -G "Unix Makefiles" \
        -DCMAKE_BUILD_TYPE=Debug \
        -DCMAKE_INSTALL_PREFIX=$PREFIX \
        -DLIB_SUFFIX= \
        -DENABLE_FILE_LOCKING=fcntl \
        -DENABLE_DOT_LOCKING=OFF \
        -DENABLE_CANBERRA=OFF \
        -DENABLE_OAUTH2=ON \
        -DENABLE_GTK=ON \
        -DENABLE_UOA=OFF \
        -DENABLE_EXAMPLES=ON \
        -DENABLE_INTROSPECTION=ON \
        -DENABLE_VALA_BINDINGS=ON \
        -DENABLE_INSTALLED_TESTS=ON \
        -DENABLE_GTK_DOC=OFF \
        -DWITH_PRIVATE_DOCS=OFF \
        -DWITH_PHONENUMBER=OFF \
	-DWITH_NSPR_INCLUDES=/usr/include/nspr/ \
        -DWITH_NSS_INCLUDES=/usr/include/nss \
        -DWITH_LIBDB=OFF
