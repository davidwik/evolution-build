#!/bin/bash
if [ -z $PREFIX ]
then
    echo "Need to source the src.sh first"
    exit 1
fi
cmake .. -G "Unix Makefiles" \
        -DCMAKE_BUILD_TYPE=Debug \
        -DCMAKE_INSTALL_PREFIX=$PREFIX \
        -DLIB_SUFFIX= \
        -DENABLE_AUTOAR=OFF \
        -DENABLE_CANBERRA=OFF \
        -DENABLE_CONTACT_MAPS=OFF \
        -DENABLE_GNOME_DESKTOP=OFF \
        -DENABLE_INSTALLED_TESTS=OFF \
        -DENABLE_PST_IMPORT=OFF \
        -DENABLE_GTK_DOC=OFF \
	-DWITH_NSPR_INCLUDES=/usr/include/nspr/ \
        -DWITH_NSS_INCLUDES=/usr/include/nss/ \
        -DWITH_GLADE_CATALOG=OFF \
        -DWITH_HELP=OFF
