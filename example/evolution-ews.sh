#!/bin/bash
if [ -z $PREFIX ]
then
    echo "Need to source the src.sh first"
    exit 1
fi
cmake .. -G "Unix Makefiles" \
        -DCMAKE_BUILD_TYPE=Debug \
        -DCMAKE_INSTALL_PREFIX=$PREFIX \
        -DLIB_SUFFIX= \
        -DWITH_MSPACK=ON
