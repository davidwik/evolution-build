#!/bin/bash

# Modify SRC_DIR and PREFIX to your liking.

export SRC_DIR="$HOME/source"
export PREFIX="$HOME/build"
export PATH=$PREFIX/bin:$PATH
export XDG_DATA_DIRS=$PREFIX/share:$XDG_DATA_DIRS
export LD_LIBRARY_PATH=$PREFIX/lib:$LD_LIBRARY_PATH
export PKG_CONFIG_PATH=$PREFIX/lib/pkgconfig:$PREFIX/share/pkgconfig:$PKG_CONFIG_PATH
export GSETTINGS_SCHEMA_DIR=$PREFIX/share/glib-2.0/schemas:$GSETTINGS_SCHEMA_DIR
export CFLAGS="-Wno-deprecated-declarations"
