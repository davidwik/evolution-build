# BUILD SCRIPTS FOR EVOLUTION #
In order to harness the most newest features off evolution 
email client you have to build it from scratch.

This is a collection of helper scripts that will make it easier 
to compile and build the newest branch of the Evolution e-mail client.
And hopefully bypass the trial and error I pain-stakingly have to 
go through. 

__NOTE__: This is an experimental build from the master branch.

## Setting up ##
1. Edit the `src.sh` so the source directory points to the directory 
this readme file is in. Change the variables SRC_DIR and PREFIX to your
liking. $PREFIX will point to the target directory of the install. If you want to install the application 
globally you can put the PREFIX as `/usr/local/`.

2. Make sure that you are using the environment variables from src.sh
`source src.sh`

2. Run install-requirements.sh to install all the build requirements.
`./install-dependencies.sh`

3. Run prepare-build.sh to create build directories and build scripts. 
`./prepare-build.sh`

## Configure and compiles ##

### 1. Ensure yet again that the env vars are loaded ###
~~~~
source src.sh
~~~~

### 2. Compile the evolution-data-server ###
~~~~
cd evolution-data-server/_build
./build.sh
make -j8 && make install
~~~~
If everything went ok, you can build it with make and install it. The _-j_ flag indicates how many jobs it will compile in parallell. There might be dependencies you need to install. If you want it install globally you need to `sudo make install`.


### 3. Compile the client ###
~~~~
cd evolution/_build
./build.sh
make -j8 && make install
~~~~

### 4. (Optional)Exchange webservices support ###
~~~~
cd evolution-ews/_build
./build.sh
make -j8 && make install
~~~~

### 5. (Optional) RSS support ###
~~~~
cd evolution-rss
./autogen.sh --prefix=$PREFIX
make && make install
~~~~

## Running Evolution ##
If you just are checking your build locally you can use the 
`run-evolution.sh` script. It starts the background services and 
registrers to dbus. If you want to use it as your daily driver, you 
can take inspiration from the startup script under `example/startup-evolution-services.sh` which can be loaded when you login. 

## Known troubles ##
In my version of Linux mint the include files for nss and nspr lies under /usr/include/nss and /usr/includes/nspr which 
caused CMAKE not to find them correctly. If your Linux dist has them elsewhere, update or remove the following variables in 
_evolution-data-server/_build/build.sh_:
~~~~
-DWITH_NSPR_INCLUDES=/usr/include/nspr
-DWITH_NSS_INCLUDES=/usr/include/nss
~~~~

