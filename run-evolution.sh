#!/bin/bash
source src.sh
read
echo "Starting background services..."
$PREFIX/libexec/evolution-source-registry &
sleep 1
$PREFIX/libexec/evolution-calendar-factory -r &
sleep 1
$PREFIX/libexec/evolution-addressbook-factory -r &
sleep 1
$PREFIX/bin/evolution &
