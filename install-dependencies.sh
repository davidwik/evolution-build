#!/bin/bash
sudo apt install gnome-common \
     libgconf2-dev \
     libnspr4-dev \
     libkrb5-dev \
     gperf \
     libgoa-1.0-dev \
     libxml2-dev \
     gobject-introspection \
     libgdata-dev \
     libgcr-3-dev \
     libjson-glib-dev \
     libgweather-3-dev \
     libldap2-dev \
     libsqlite3-dev \
     libsecret-1-dev \
     libwebkit2gtk-4.0-37 \
     libnss3-dev \
     libwebkit2gtk-4.0-dev \
     libical-dev \
     libgirepository1.0-dev \
     valac \
     libenchant-2-dev \
     gsettings-desktop-schemas-dev \
     libgspell-1-dev \
     libcryptui-dev \
     libnotify-dev \
     libytnef0-dev \
     highlight \
     libgail-3-dev \
     libmspack-dev \
     libcamel1.2-dev
